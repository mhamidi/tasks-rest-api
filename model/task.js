const mongoose = require('mongoose');

const Task = mongoose.model('Task', {
  text: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 30,
    trim: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  completedAt: {
    type: Number,
    default: null
  }
});

module.exports = Task;
