const mongoose = require('mongoose');

const User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 30,
    trim: true
  },
  age: {
    type: Number
  },
  location: {
    type: String,
    default: null
  }
});

module.exports = User;
