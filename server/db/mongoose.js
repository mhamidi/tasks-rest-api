const mongoose = require('mongoose');
// Why to specify that we need use Promise ?
// Because mongoose, by default use callbacks, we will use built-in promises
mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);

const dbName = 'tasksdb';
const port = 27017;
const url = process.env.MONGODB_URI || `mongodb://localhost:${port}/${dbName}`;
mongoose.connect(url, { useNewUrlParser: true }, (error) => {
  if(error) {
    return console.log('Unable to connect to mongo db server', error);
  }  
});

console.log('Connected to mongodb server..');

module.exports = {
  mongoose
};
