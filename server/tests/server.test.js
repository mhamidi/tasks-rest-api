const expect = require('expect');
const request = require('supertest');
const { ObjectID } = require('mongodb');

const { app } = require('../server');
// const User = require('../../model/user');
const Task = require('../../model/task');

const tasks = [
  { _id: new ObjectID(), text: 'First test task'},
  { _id: new ObjectID(), text: 'Second test task'},
  { _id: new ObjectID(), text: 'Third test task', completed: true, completedAt: 333}
];

beforeEach((done) => {
  Task.deleteMany({}).then(
    () => {
      return Task.insertMany(tasks);
    }).then(() => done());
});

describe('POST /tasks', () => {
  it('should return a new created task', (done) => {
    const text = 'Continue Learn Nodejs';
    request(app)
      .post('/tasks')
      .send({ text })
      .expect(200)
      .expect((res) => {
        expect(res.body.text).toBe(text);
      })
      .end((error, res) => {
        if(error) {
          return done(error);
        }

        Task.find({text}).then((tasks) => {
          expect(tasks.length).toBe(1);
          expect(tasks[0].text).toBe(text);
          done();
        }).catch((error) => done(error));
      });
  });

  it('should not create task with invalid data', (done) => {
    request(app)
      .post('/tasks')
      .send({})
      .expect(400)
      .end((error, res) => {
        if(error) {
          return done(error);
        }

        Task.find().then(tasks => {
          expect(tasks.length).toBe(3);
          done();
        }).catch((error) => done(error));
      });

  });
});


describe('GET /tasks', () => {
  it('should return no task from db', (done) => {
    request(app)
      .get('/tasks')
      .expect(200)
      .expect((res) => {
        expect(res.body.tasks.length).toBe(3);
      })
      .end(done);
  });
});


describe('Get /tasks/:id', () => {
  it('should return one task', (done) => {
    request(app)
      .get(`/tasks/${tasks[0]._id.toHexString()}`)
      .expect(200)
      .expect(
        res => {
          expect(res.body.task.text).toBe(tasks[0].text)
        }
      )
      .end(done);
  });

  it('should return non task (task not found)', (done) => {
    request(app)
      .get(`/tasks/${new ObjectID().toHexString()}`)
      .expect(404)
      .end(done);
  });

  it('should return 404 for non valid ObjectID', (done) => {
    request(app)
      .get('/tasks/12345')
      .expect(404)
      .end(done);
  });
});

describe('DELETE /tasks/:id', () => {
  it('sould delete a task with a given id', (done) => {
    const taskid = tasks[0]._id;
    request(app)
      .delete(`/tasks/${taskid}`)
      .expect(200)
      .expect(res => {
        expect(res.body.task.text).toBe(tasks[0].text)
      }).end((err, res) => {
        if(err) {
          return done(err);
        }
        Task.findById(taskid).then(
          task => {
            expect(task).toBeFalsy();
            done();
          }
        ).catch(e => done(e));
      });
  });

  it('should delete non task (task not found)', (done) => {
    request(app)
      .delete(`/tasks/${new ObjectID().toHexString()}`)
      .expect(404)
      .end(done);
  });

  it('should return 404 for non valid ObjectID', (done) => {
    request(app)
      .delete('/tasks/12345')
      .expect(404)
      .end(done);
  });

});

describe('PATCH /tasks/:id', () => {
  it('should update the task', (done) => {
    const taskid = tasks[0]._id;
    const text = 'updated from test';
    request(app)
      .patch(`/tasks/${taskid}`)
      .send({text, completed: true})
      .expect(200)
      .expect(res => {
        expect(res.body.task.text).toBe(text);
        expect(res.body.task.completed).toBeTruthy();
        expect(typeof res.body.task.completedAt).toBe('number');
      }).end(done)
  });

  it('should clean completedAt when completed is false', (done) => {
    const taskid = tasks[2]._id;
    const text = 'updated from test bis!!';
    request(app)
      .patch(`/tasks/${taskid}`)
      .send({text, completed: false})
      .expect(200)
      .expect(res => {
        expect(res.body.task.text).toBe(text);
        expect(res.body.task.completed).toBeFalsy();
        expect(res.body.task.completedAt).toBeFalsy();
      }).end(done)
  });
})