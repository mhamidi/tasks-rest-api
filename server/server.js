const express = require('express');
const _ = require('lodash');
const bodyParser = require('body-parser');
const { ObjectID } = require('mongodb');

const {mongoose} = require('./db/mongoose.js');
const User = require('../model/user');
const Task = require('../model/task');

const app = express();
const port = process.env.PORT || 3000;
// add bodyParser middleware
app.use(bodyParser.json());

app.post('/tasks', (req, res) => {
  const task  = new Task(req.body);
  task.save().then(
    doc => {
      res.status(200).send(doc);
    },
    error => {
      res.status(400).send(error);
    }
  );
});

app.get('/tasks', (req, res) => {
  Task.find().then(
    tasks => {
      res.status(200).send({tasks});
    },
    error => {
      res.status(400).send(error);
    }
  );
});

app.get('/tasks/:id', (req, res) => {
  const taskid = req.params.id;
  if(!ObjectID.isValid(taskid)) {
    return res.status(404).send();
  }

  Task.findById(taskid).then(
    task => {
      if(!task) {
        return res.status(404).send('No task found');
      }
      res.status(200).send({task})
    },
    error => res.status(400).send(error.message)
  );
});

app.delete('/tasks/:id', (req, res) => {
  const taskid = req.params.id;
  if(!ObjectID.isValid(taskid)) {
    return res.status(404).send();
  }
  Task.findByIdAndDelete(taskid).then(
    task => {
      if(!task) {
        return res.status(404).send('No task belong to the id ' + taskid);
      }
      res.status(200).send({task});
    },
    error => res.status(400).send('Error when deleting the task ' + taskid)
  );
});

app.patch('/tasks/:id', (req, res) => {
  const taskid = req.params.id;
  const body = _.pick(req.body, ['text', 'completed']);
  if(!ObjectID.isValid(taskid)) {
    return res.status(404).send();
  }

  if(_.isBoolean(body.completed) && body.completed) {
    body.completedAt = new Date().getTime();
  } else {
    body.completed = false;
    body.completedAt = null;
  }
  Task.findByIdAndUpdate(taskid, {$set: body}, {new: true}).then(
    task => {
      if(!task) {
        return res.status(404).send();
      }
      res.status(200).send({task});
    }).catch(e => res.status(400).send());
});

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

module.exports = {
  app
}
