const { ObjectID } = require('mongodb');
const { mongoose } = require('../server/db/mongoose');
const Task = require('../model/task');

const id = '5bcb521ae2279b03198e3010';

if(!ObjectID.isValid(id)) {
  console.log('id is not valid');
}

/* Task.deleteMany({}).then(
  result => console.log(result) 
); */

Task.findByIdAndDelete(id).then(
  task => {
    if(!task) {
      return console.log('No task found by this id', id);
    }
    console.log(task)
  },
  error => console.log(error)
);
