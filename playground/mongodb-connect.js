// const MongoClient = require('mongodb').MongoClient;
const { MongoClient, ObjectID } = require('mongodb');

// const objId = new ObjectID();
// console.log(objId, objId.getTimestamp());


const url = 'mongodb://localhost:27017/';
const dbName = 'tasksdb';

MongoClient.connect(url, { useNewUrlParser: true }, (error, client) => {
  if(error) {
    return console.log('Error when connecting to database server', error);
  }
  const db = client.db(dbName);
  db.collection('tasks').insertOne(
    {
      text: 'Learn Nodejs',
      completed: false
    }, (error, result) => {
      if(error) {
        return console.log('Error when creating the task', error);
      }
      console.log(result.ops);
      client.close();
    })

    db.collection('users').insertOne(
      {
        name: 'Mohamed',
        age: 37,
        location: 'Casablanca'
      }, (error, result) => {
        if(error) {
          return console.log('Error when creating the user', error);
        }
        console.log(result.insertedId.getTimestamp());
        client.close();
      })
});
