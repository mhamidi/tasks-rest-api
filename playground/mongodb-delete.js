const { MongoClient, ObjectID } = require('mongodb');

const url = 'mongodb://localhost:27017';
const dbName = 'tasksdb';

MongoClient.connect(url, { useNewUrlParser: true }, (error, client) => {
  if(error) {
    return console.log('Unable to connect to the db server', error);
  }

  console.log('Connected to mongo db server');
  const db = client.db(dbName);
  const criteria = { text: 'Learn Nodejs' }
  
  // Delete Many Objects that match criteria
  db.collection('tasks').deleteMany(criteria).then(
    result => console.log(result.result)
  );

  // Delete the first object that match criteria
  db.collection('tasks').deleteOne(criteria).then(
    result => console.log(result)
  );

  // Find a specific object and delete it
  /* db.collection('tasks').findOneAndDelete(criteria).then(
    result => console.log(result)
  ); */

  client.close();
});
