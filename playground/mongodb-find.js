const { MongoClient, ObjectID } = require('mongodb');

const url = 'mongodb://localhost:27017/';
const dbName = 'tasksdb';

MongoClient.connect(url, { useNewUrlParser: true }, (error, client) => {
  if(error) {
    return console.log('Error when connecting to database server', error);
  }
  const db = client.db(dbName);
  const conds = {completed: false};
  db.collection('tasks').find(conds).count().then(count => console.log(`There are ${count} tasks`));
  db.collection('tasks').find(conds).toArray().then(
    // tasks => tasks.map(task => console.log(task._id)),
    tasks => console.log(JSON.stringify(tasks, ['text', 'completed', '_id'], 2)),
    error => console.log('Unable to fetch data', error)
  );
  client.close();
});
