const { ObjectID } = require('mongodb');
const { mongoose } = require('./../server/db/mongoose');
const Task = require('./../model/task');

const id = '5bca06d25b6ac228c48b3a96';

if(!ObjectID.isValid(id)) {
  console.log('id is not valid');
}

/* Task.find({
  _id: id
}).then(tasks => {
  console.log(tasks);
});

Task.findOne({
  _id: id
}).then(task => {
  console.log(task);
});  */

Task.findById(id).then(
  task => {
    if(!task) {
      return console.log('Error1');
    } 
    console.log('task:', task)
  },
  error => console.log('Error:', error.message)
).catch(error => console.log('Error2', error));
