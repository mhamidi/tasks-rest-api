const { MongoClient, ObjectID } = require('mongodb');

const url = 'mongodb://localhost:27017';
const dbName = 'tasksdb';

MongoClient.connect(url, { useNewUrlParser: true }, (error, client) => {
  if(error) {
    return console.log('Unable to connect to the db server', error);
  }

  console.log('Connected to mongo db server');
  const db = client.db(dbName);
  const criteria = { _id: new ObjectID('5bc7a4fe0ce6e8e79984d45d') }
  
  db.collection('tasks').findOneAndUpdate(criteria, {$set : {completed: false}}, { returnOriginal: false }, 
    (error, result) => {
      if(error) {
        return console.log('Unable to update the object', error);
      }
      console.log(result)
    });

  client.close();
});
